# README -- Interface graphique **Simplexe**

Projet r�alis� par :

* **Claire BAUCHU**
* **Martin-Stephen NIOMBELA**
* **Damien CHANCEREL**
* **Victor CHARD�RON**
* **Louis BIZOT**

> Ann�e 2018 - 2019

### Pourquoi ce repository ?

> Quick summary

Ce repository contient notre projet de 2�me ann�e de DUT Informatique. Ce projet consistait � impl�menter une interface graphique
pouvant servir � des �l�ves pour r�soudre des _simplexes_.

Nous avons aussi collabor� avec un autre groupe de projet, afin de fournir un logiciel contenant 2 interfaces graphiques : la n�tre pour les _simplexes_ et celle 
de notre groupe partenaire pour les _matrices_.

### Le code

> Notre code

Notre code poss�de sa javadoc ainsi que des commentaires pour les passages un peu obscurs de certaines classes. Cependant, nous estimons que notre code est assez
lisible et compr�hensible � la premi�re lecture. 

Notre projet ne n�cessite pas de configuration particuli�re. Pour avoir les classes de ce repository, il suffit seulement d'avoir Eclipse (version _Oxygen_) et de cloner
ce repository (il est pour l'instant en priv�). 

### Utilisation de l'application

> T�l�charger l'application compl�te 

Pour t�l�charger l'application que nous avons d�velopp�, vous trouverez le **.jar** [ici](https://bitbucket.org/cbauchu/simplexe/downloads/) (_Simplix.jar_).

> Manuel utilisateur

Notre application est facile d'utilisation et accessible � tous. Cependant, en cas de non-compr�hension d'une fonctionnalit�, vous pourrez toujours vous r�f�rer
� notre manuel utilisateur (assez succinct) [ici](https://bitbucket.org/cbauchu/simplexe/src/387e00e4e08ed946f6cd8055fc30a60715d515a7/manuel.md?at=master).
Nous le laissons au format **.md**, dans l'�ventualit� o� un utilisateur voudrait le modifier.

### Un soucis ?

En cas de soucis, vous pouvez envoyer un mail � cette adresse : **niombela.martin@gmail.com** .